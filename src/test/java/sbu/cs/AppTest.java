package sbu.cs;

import org.junit.Test;
import org.junit.Assert.*;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class AppTest {

    private static String dir = "C:\\Users\\Mostafa\\Desktop\\Test\\";
    private static String pdf = "C:\\Users\\Mostafa\\Desktop\\series-7\\book.pdf";
    private static String png = "C:\\Users\\Mostafa\\Desktop\\series-7\\sbu.png";

    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    /*
    @Test
    public void transferPDF() {
        try {
            Server fts = new Server(dir);
            fts.start();
            Thread.sleep(2000);
            byte[] f1 = Files.readAllBytes(Paths.get(pdf));
            FTClient ftc = new FTClient(pdf);
            ftc.start();
            Thread.sleep(5000);
            byte[] f2 = Files.readAllBytes(Paths.get(dir + "book.pdf"));
            assertArrayEquals(f1,f2);
        }
        catch (Exception e){
            fail();
        }
    }

    @Test
    public void transferPNG() {
        try {
            Server fts = new Server(dir);
            fts.start();
            Thread.sleep(2000);
            byte[] f1 = Files.readAllBytes(Paths.get(png));
            FTClient ftc = new FTClient(png);
            ftc.start();
            Thread.sleep(5000);
            byte[] f2 = Files.readAllBytes(Paths.get(dir + "sbu.png"));
            assertArrayEquals(f1,f2);
        }
        catch (Exception e){
            fail();
        }
    }
    @Test
    public void mutlithreadedTest() {
        try {
            Server fts = new Server(dir);
            fts.start();
            Thread.sleep(2000);
            byte[] f1pdf = Files.readAllBytes(Paths.get(pdf));
            FTClient ftcPDF = new FTClient(pdf);
            byte[] f1png = Files.readAllBytes(Paths.get(png));
            FTClient ftcPNG = new FTClient(png);
            ftcPNG.start();
            ftcPDF.start();
            Thread.sleep(5000);
            byte[] f2pdf = Files.readAllBytes(Paths.get(dir + "book.pdf"));
            byte[] f2png = Files.readAllBytes(Paths.get(dir + "sbu.png"));
            assertArrayEquals(f1pdf,f2pdf);
            assertArrayEquals(f1png,f2png);
        }
        catch (Exception e){
            fail();
        }
    }
    */
}
