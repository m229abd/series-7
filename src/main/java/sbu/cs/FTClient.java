package sbu.cs;

import java.io.*;
import java.net.Socket;

public class FTClient extends Thread{
    String filePath;
    public FTClient(String filePath){
        this.filePath = filePath;
    }
    @Override
    public void run() {
        try {
            //Get file name and format:
            String[] fileName = filePath.split("[\\\\/]");
            System.out.println("Client : Connecting to server");
            //Connect to server:
            Socket socket = new Socket("127.0.0.1", 6500);
            System.out.println("Client : Connected");
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            System.out.println("Client : Transferring file name");
            //Send file name:
            dos.writeUTF(fileName[fileName.length - 1]);
            dos.flush();
            //Send file size:
            System.out.println("Client : Transferring file size");
            File f = new File(filePath);
            dos.writeInt((int)(f.length()));
            dos.flush();
            System.out.println("Client : File transfer started");
            FileInputStream fis = new FileInputStream(f);
            byte[] buffer = new byte[4096];
            while (fis.read(buffer) > 0){
                dos.write(buffer);
                dos.flush();
            }
            System.out.println("Client : File transfer finished, closing everything...");
            fis.close();
            dos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
