package sbu.cs;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class FTServer extends Thread{

    private String dir;
    private Socket socket;

    public FTServer(String directory, Socket socket){
        dir = directory;
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            System.out.printf("Server %d: Client connected", this.getId());
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            //Get file name:
            String fileAddress = dir + dis.readUTF();
            System.out.printf("Server %d : File name is : %s%n", this.getId(), fileAddress);
            //Get file size:
            int fileSize = dis.readInt();
            System.out.printf("Server %d : File size is : %d%n", this.getId(), fileSize);
            File f = new File(fileAddress);
            //If file doesn't exist, create it:
            if (f.createNewFile())
                System.out.printf("Server %d : File created%n", this.getId());
            else{
                //must NOT rewrite existing files:
                System.out.printf("Server %d : File already exists%n" , this.getId());
                dis.close();
                socket.close();
                System.out.printf("Server %d : Aborted%n" , this.getId());
            }
            //Start file transfer:
            FileOutputStream fos = new FileOutputStream(f);
            System.out.printf("Server %d : Writing to file%n", this.getId());
            byte[] buffer = new byte[4096];
            int remaining = fileSize;
            System.out.printf("Server %d : File transfer started%n", this.getId());
            for (int read = 0; (read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0; remaining -= read)
                fos.write(buffer, 0, read);
            System.out.printf("Server %d : File transfer finished, closing everything...%n", this.getId());
            //Close everything!
            fos.close();
            dis.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
