package sbu.cs;


import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread{

    static private String directory;

    public Server(String dir){
        directory = dir;
    }

    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // below is the name of directory which you must save the file in it
        directory = args[0] + "/";     // default: "server-database"
    }

    public void run(){
        try {
            ServerSocket ss = new ServerSocket(6500);
            while(true){
                System.out.println("Server : Waiting for a client to connect");
                FTServer fts = new FTServer(directory,ss.accept());
                fts.setDaemon(true);
                fts.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
